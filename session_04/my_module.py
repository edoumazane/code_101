def say_hello(to_name, prints=True, returns=False):
    if prints:
        print(f"Hello, {to_name}!")
    if returns:
        return f"Hello, {to_name}!"
