# Commands

```bash
# Create a new directory
mkdir mydir
# Create a new file
touch myfile
# List files and directories
ls
# Change directory
cd mydir
# Change to parent directory
cd ..
# Change to root
cd /
# Change to home directory
cd ~
# Path to current directory
pwd
# Remove file
rm myfile
# Remove directory
rmdir mydir
# Or
rm -r mydir
# Copy file to another file
cp myfile mycopy
# Copy file to another directory
cp myfile mydir
# Rename a file
mv myfile mynewfile
# Move a file to another directory
mv myfile mydir
# Move a directory to another directory
mkdir mynewdir
mv mydir mynewdir

# Print an environment variable
echo $HOME
echo $HOSTNAME (on a Mac: echo $HOST)
echo $USERNAME
# Look up the IP address of a domain
nslookup google.com
nslookup icm-renie-lf010
# Connect to a remote server
ssh my_user_name@my_remote_machine #change with your username and remote server
# Shortcut to deconnect: Ctrl + D
# Other shortcuts: Ctrl + C to stop a command
# Ctrl + Shift + C to copy (or Cmd + C on a Mac),
# Ctrl + Shift + V to paste (or Cmd + V on a Mac),
# Ctrl + Shift + T to open a new tab (or Cmd + T on a Mac),
# Ctrl + Shift + W to close a tab (or Cmd + W on a Mac),
# Ctrl + Shift + Q to close the terminal (or Cmd + Q on a Mac).
# Ctrl + L to clear the terminal

# Copy files to a remote server
scp myfile my_user_name@my_remote_machine:~/mydir

# The other way around
scp my_user_name@my_remote_machine:~/mydir/myfile .


# Show the path of a command
which ls # or which nano or which python


# Create or edit a text file
nano myfile

# Print the content of a file
cat myfile

# Print the content of a file with line numbers
nl myfile

# Print the beginning of a file
head myfile

# Print the end of a file
tail myfile





# Task to do

# Type the following command
hello
# You should see an error message
# Edit or create a file that manages the aliases at your home directory
nano ~/.bash_aliases # or nano ~/.zsha
# Add the following line
alias hello='echo "Hello, World!"'
# Save the file and close the editor
# Open a new terminal or a new tab
# Type the following command
hello





# Examples of package managers
pip
apt
brew
conda # it is also an environment manager

# Try this out:
which pip # what happens?
conda activate ClearMapUi39
which pip # what happens?

```
